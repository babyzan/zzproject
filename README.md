#  赞赞框架(持续更新中)

#### 介绍
这是一个整合了多种插件并添加了多种实用功能的基于ElementUI的框架，包括动态报表设置、数字签名、工作流管理、chatGPT、3D建模展示、直播流、动态新增、动态查看、excel表格导入到处、动态打印等功能和插件，主要用于后台管理系统的开发，可以让程序员更方便快捷地开发WMS,ERP等系统。
github同步更新(https://github.com/babyzanzan/zzproject)

#### 展示页面
点击进入显示页面(http://47.99.40.118:8079/)

#### 软件架构
系统前端：elementUI-admin
系统后端：暂无关联后端
数据库：mysql

#### 安装教程

1.  克隆项目 git clone https://gitee.com/babyzan/zzproject.git
2.  进入项目目录 cd zzproject/vue-admin-template-master
3.  使用 npm install 注入依赖
4.  使用 npm run dev 进入开发模式
5.  使用 npm run build:prod 对项目进行打包，输出文件在dist

#### 使用说明

1.  本项目面向 所有人
2.  现版本为1.0版本

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  进入作者页面[作者](https://gitee.com/babyzan/projects?sort=&scope=&state=private)